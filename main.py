# TensorConcerto - Generate live music in Python
# Copyright (C) 2018  belugawhale

# Imports
import tensorflow as tf
from tensorflow.python.saved_model import tag_constants
from MidiHandler import *


# Command line class, processes user input
class CommandLine:

    # Standard init function
    def __init__(self):
        print("""    TensorConcerto  Copyright (C) 2018 belugawhale
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.""")


# Model Learner thing
def learn(msg_list):
    t_input = tf.placeholder('float', shape=[None, 1], name="t_input0")
    target = tf.placeholder('float', shape=[None, 1], name="target")
    input_bias = tf.Variable(initial_value=tf.random_normal(shape=[1], stddev=0.4),
                             dtype='float', name="input_bias")

    waits = tf.Variable(initial_value=tf.random_normal(shape=[1, 3], stddev=0.4), dtype='float', name="hidden_waits")
    hidden_bias = tf.Variable(initial_value=tf.random_normal(shape=[1], stddev=0.4), dtype='float', name="hidden_bias")

    output_weights = tf.Variable(initial_value=tf.random_normal(shape=[3, 1],
                                                                stddev=0.4), dtype='float', name="output_weights")

    hidden_layer = tf.matmul(t_input, waits) + input_bias
    hidden_layer = tf.sigmoid(hidden_layer, name="activation")

    output = tf.matmul(hidden_layer, output_weights) + hidden_bias
    hidden_layer = tf.sigmoid(output, name="activation")

    cost = tf.squared_difference(target, output)
    cost = tf.reduce_mean(cost)
    optimizer = tf.train.AdamOptimizer().minimize(cost)

    epochs = 16

    with tf.Session() as session:
        tf.global_variables_initializer().run()
        for i in range(epochs):
            for e in range(len(msg_list)):
                inp = [[msg_list[e][2]], [msg_list[e][3]], [msg_list[e][4]]]
                out = [[msg_list[e][2]], [msg_list[e][3]], [msg_list[e][4]]]
                err, _ = session.run([cost, optimizer], feed_dict={t_input: inp, target: out})
                print("ITERATION:" + str(i) + " ERRORS:" + str(err))
        tf.saved_model.simple_save(
            session, 'saved_model'
        )


# Message list generator
def generate_message_list(old_message_list):
    message_list = []
    with tf.Session() as session:
        tf.saved_model.loader.load(
            session
            [tag_constants.SERVING],
            'saved_model',
        )
        for e in range(len(old_message_list)):
            inp = [old_message_list[e][2], old_message_list[e][3], old_message_list[e][4]]
            result = session.run([output], feed_dict={Input: inp})
            message_list.append(result)
