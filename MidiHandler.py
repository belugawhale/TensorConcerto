# TensorConcerto - Generate live music in Python
# Copyright (C) 2018  belugawhale

# Imports
import os
import random
import mido


# Midi Folder class
class MidiFolder:

    # Standard init function
    def __init__(self, folder_path, extension_name, ignore_list):
        self.path, self.extension, self.ignore = folder_path, extension_name, ignore_list

    # Get a midi file in the MidiFile class format
    def get_random_midi_formatted(self):
        midi_files = []
        for sub_dirs, dirs, files in os.walk(self.path):
            for file in files:
                file_path = sub_dirs + os.sep + file
                if file_path.endswith(self.extension):
                    midi_files.append(file_path)
        midi_file_path = random.choice(midi_files)
        midi_file = mido.MidiFile(midi_file_path)
        return midi_file

    # Get the midi file path
    def get_random_midi_path(self):
        midi_files = []
        for sub_dirs, dirs, files in os.walk(self.path):
            for file in files:
                file_path = sub_dirs + os.sep + file
                if file_path.endswith(self.extension):
                    midi_files.append(file_path)
        midi_file_path = random.choice(midi_files)
        return midi_file_path


# Message list getter
def get_message_list(midi_file):
    midi_message_list = []
    for msg in midi_file:
        if not msg.is_meta:
            midi_message_list.append(str(msg).split())
    return midi_message_list
